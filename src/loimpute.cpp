/*
 * loimpute.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: jkpickrell
 */
#include "Haplotypes.h"
#include "Pileup.h"

void printv(){
	cout << "loimpute v. 0.01\n";
	cout << "Joe Pickrell (joepickrell@gmail.com)\n\n";
}

void printopts(){
	cout << "Options:\n";
	cout << "\nImputation mode:\n";
	cout << "-i [pileup file name] input pileup file for imputation\n";
	cout << "-p [input model stem] stem of file names with model to input\n";
	cout << "-id [string] individual identifier for output (default to same as -o)\n";
	cout << "-e [float] assumed sequencing error rate (default: 1e-3)\n";
	cout << "-m [int] maximum number of reads per site (default: 2)\n";
	cout << "-min [int] minimum number of variants overlapping the pileup and reference to avoid an error (default: 1000)\n";
	cout << "-range [int_min] [int_max] in the output, only print positions in the range [int_min, int_max]. (default: not enabled)\n";
	cout << "\nParameter estimation mode:\n";
	cout << "-h [VCF file name] reference haplotype file\n";
	cout << "-k [int] number of haplotype clusters (default to 20)\n";

	cout << "-it [int] number of EM iterations (default to 10)\n";
	cout << "-nt [int] number of threads (if OpenMP is available, defaults to 1)\n";
	cout << "\nShared:\n";
	cout << "-s [int] set random seed (default to system time)\n";
	cout << "-o [string] output stem (default: loimpute)\n";


	cout << "\n";
}


int main(int argc, char *argv[]){
	printv();
    CCmdLine cmdline;
    Loimpute_params p;
    if (cmdline.SplitLine(argc, argv) < 1){
    	printopts();
    	exit(1);
    }
    if (cmdline.HasSwitch("-h")) {
    	p.imputationmode = false;
    	p.hapfile = cmdline.GetArgument("-h", 0);
    }
    if (cmdline.HasSwitch("-i")) {
    	p.imputationmode = true;
       	p.readpileup = true;
       	p.pileupfile = cmdline.GetArgument("-i", 0);
    }
    if (cmdline.HasSwitch("-h") && cmdline.HasSwitch("-i")){
    	printopts();
    	cerr << "ERROR: can only choose one of -h or -i\n";
    	exit(1);
    }
    if (!cmdline.HasSwitch("-h") && !cmdline.HasSwitch("-i")){
    	printopts();
    	cerr << "ERROR: either -h or -i is required\n";
    	exit(1);
    }
    if (cmdline.HasSwitch("-nt") && !p.imputationmode){
		#ifdef _OPENMP
    		p.nthread = atoi(cmdline.GetArgument("-nt", 0).c_str());
    		int mthread = omp_get_max_threads();
    		if (p.nthread > mthread) {
    			p.nthread = mthread;
    			cout << "Max number of threads is "<< mthread << "\n";
    		}
    		omp_set_num_threads(p.nthread);
		#else
    		cout << "OpenMP not available, running on a single thread\n";
		#endif
    }
    if (cmdline.HasSwitch("-hh")) p.read_vcf = false;

    //output
    if (cmdline.HasSwitch("-o")) {
    	p.outstem = cmdline.GetArgument("-o", 0);
    	p.samplename = p.outstem;
    }
    if (cmdline.HasSwitch("-id")) {
    	p.samplename = cmdline.GetArgument("-id", 0);
    }

    //
    if (cmdline.HasSwitch("-p") && p.imputationmode) {
    	p.read_params = true;
    	p.paramstem = cmdline.GetArgument("-p", 0);
    }
    else if (!cmdline.HasSwitch("-p") && p.imputationmode) {
    	printopts();
    	cerr << "ERROR: Need to provide parameters with -p if running imputation with -i\n";
    	exit(1);

    }
    if (cmdline.HasSwitch("-s")){
      	p.seed = atoi(cmdline.GetArgument("-s", 0).c_str());
    }
    else  p.seed = (int) time(0);
    if (cmdline.HasSwitch("-e")){
    	p.erate = atof(cmdline.GetArgument("-e", 0).c_str());
    }
    if (cmdline.HasSwitch("-m")){
    	p.maxreads = atoi(cmdline.GetArgument("-m", 0).c_str());
    }
    if (cmdline.HasSwitch("-k") && !p.imputationmode)  	p.K = atoi(cmdline.GetArgument("-k", 0).c_str());
    if (cmdline.HasSwitch("-min"))  	p.minoverlap = atoi(cmdline.GetArgument("-min", 0).c_str());
    if (cmdline.HasSwitch("-range"))  	{
    	p.printrange = true;
    	p.minprint = atoi(cmdline.GetArgument("-range", 0).c_str());
    	p.maxprint = atoi(cmdline.GetArgument("-range", 1).c_str());
    }
    if (cmdline.HasSwitch("-it") && !p.imputationmode)  	p.niter = atoi(cmdline.GetArgument("-it", 0).c_str());

    //random number generator
    const gsl_rng_type * T;
    gsl_rng * r;
    gsl_rng_env_setup();
    T = gsl_rng_ranlxs2;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, p.seed);

    // print the parameter settings
    p.print();
    if (!p.imputationmode){
    	Haplotypes h(&p, r);
    	h.EM(p.niter);
        cout << "Printing parameters\n";
        h.print_params();
    }

    if (p.imputationmode){
       	Haplotypes h(&p, r);
    	Pileup pu(&p, &h);
    	pu.impute();
    	cout << "Printing imputed VCF\n";
    	pu.write_imputed_vcf();
    }

    return 0;

}


