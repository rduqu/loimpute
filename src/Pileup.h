/*
 * Pileup.h
 *
 *  Created on: Dec 27, 2016
 *      Author: jkpickrell
 */

#ifndef PILEUP_H_
#define PILEUP_H_
#include "Loimpute_params.h"
#include "Haplotypes.h"
class Pileup{
public:
	Pileup();
	Pileup(Loimpute_params *, Haplotypes*);
	Loimpute_params *p;
	Haplotypes *h;
	vector<vector<bool> > d;
	vector<pair<string, int> > info;
	vector<pair<string, string> > als;
	map<string, map<int, int> > chr2pos2index;
	vector<vector<bool> > d_wmissing;
	vector<pair<string, int> > info_wmissing;
	vector<vector<double> > holdfwd, holdbwd;
	vector<vector<double> > gt_lk, gt_lk_null;
	vector<vector<double> > state_lk;

	double llk;
	int nsnp;
	void read_infile(string);
	void print();
	void combine_w_haplotypes();
	void init();
	double setfwd4();
	double setfwd(bool null = false);
	double setbwd(bool null = false);
	void set_null();
	double lemit(int, int, int);
	void print_fwd();
	void print_bwd();
	void impute();
	vector<double> impute(int, bool null = false);
	double readp_giveng(int, int);
	double lemit_g(int, int, int, int);
	void impute_state();
	void write_imputed_vcf();
	double kl(int);

};

#endif /* PILEUP_H_ */
