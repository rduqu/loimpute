/*
 * Loimpute_params.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: jkpickrell
 */

#include "Loimpute_params.h"

Loimpute_params::Loimpute_params(){
	autosomal_ploidy = 2;
	sexchrom = false;
	outstem = "loimpute";
	samplename = "loimpute";
	K = 20;
	read_vcf= true;
	paramstem = "";
	read_params = false;
	niter = 10;
	epsilon = 1e-6;
	nthread = 1;
	readpileup = false;
	erate = 1e-3;
	imputationmode = false;
	maxreads = 2;
	minoverlap = 1000;
	printrange = false;
}

void Loimpute_params::print(){
	cout << ":: mode: ";
	if (imputationmode) cout << "imputation" << "\n";
	else cout << "estimation" << "\n";
	if (imputationmode){
		cout << ":: pileup file: "<<  pileupfile << "\n";
		cout << ":: parameter file stem: "<< paramstem<< "\n";
		cout << ":: maxreads: "<< maxreads<< "\n";
		cout << ":: erate: "<< erate<< "\n";
		if (printrange){
			cout << ":: output range: "<< minprint << " "<< maxprint << "\n";
		}
	}
	else{
		cout << ":: haplotype file: "<< hapfile << "\n";
		cout << ":: number of clusters : "<< K << "\n";
		cout << ":: EM iterations : "<< niter << "\n";
		cout << ":: threads : "<< nthread << "\n";
	}
	cout << ":: output stem: "<< outstem << "\n";
	cout << ":: random number seed : "<< seed << "\n";


}
