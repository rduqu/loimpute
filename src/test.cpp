/*
 * test.cpp
 *
 *  Created on: Dec 22, 2016
 *      Author: jkpickrell
 */

#include "Haplotypes.h"

int main(int argc, char *argv[]){

    Loimpute_params p;
    p.hapfile = "test/Rsim.gz";
    p.read_vcf = false;
    p.read_params = true;
    p.paramstem ="test/in";
    p.seed = 10;
    p.K =2;

    //random number generator
    const gsl_rng_type * T;
    gsl_rng * r;
    gsl_rng_env_setup();
    T = gsl_rng_ranlxs2;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, p.seed);

    // print the parameter settings
    p.print();

    Haplotypes h(&p, r);

    cout << h.fwdlogp_linear(0) << " fwd linear\n"; cout.flush();
    cout << h.fwdlogp(0) << " fwd quad\n"; cout.flush();
    h.print_fwd();
    cout << h.bwdlogp_linear(0) << " bwd linear\n"; cout.flush();
    cout << h.bwdlogp(0) << " bwd quad\n"; cout.flush();
    h.print_bwd();
    assert(( (h.fwdlogp_linear(0)- h.bwdlogp_linear(0)) * (h.fwdlogp_linear(0)- h.bwdlogp_linear(0) )) < 0.00000000001);
    assert(( (h.fwdlogp(0)- h.fwdlogp_linear(0)) * (h.fwdlogp(0)- h.fwdlogp_linear(0) )) < 0.00000001);
    assert(( (h.bwdlogp(0)- h.bwdlogp_linear(0)) * (h.bwdlogp(0)- h.bwdlogp_linear(0) )) < 0.00000001);
   // assert(( (h.holdfwd[0][0] - -6.90776) * (h.holdfwd[0][0] - -6.90776)) < 0.00001);
   // assert(( (h.holdfwd[1][0] - -10.8829) * (h.holdfwd[1][0] - -10.8829)) < 0.00001);


    //cout <<h.llk() << "\n";
    h.EM(10);
    //cout <<h.llk() << "\n";
    h.print_params();
    return 0;

}
