/*
 * haplotypes.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: jkpickrell
 */

#include "Haplotypes.h"


Haplotypes::Haplotypes(){

}

Haplotypes::Haplotypes(Loimpute_params * p, gsl_rng *tmpr){
	params = p;
	r = tmpr;
	if (!p->imputationmode){
		if (params->read_vcf) read_haplotypes(params->hapfile);
		else read_haplotypes_flatfile(params->hapfile);
		cout<< "nsnp: "<< nsnp << "\n";
		cout<< "nhap: "<< nhap << "\n";
		cout.flush();
		init_params();
		cout << "Parameters initialized\n"; cout.flush();
		//if (params->read_params) read_params(params->paramstem);
		cout << "Initializing done\n";
	}
	else{
		setmodelfromfiles(params->paramstem);
	}

}

void Haplotypes::setmodelfromfiles(string instem){
	alpha.clear();
	rrate.clear();
	theta.clear();
	setalphafromfile(instem+".alphaout.gz");
	setthetafromfile(instem+".thetaout.gz");
	setrratefromfile(instem+".rout.gz");
}

void Haplotypes::setalphafromfile(string infile){
	alpha.clear();
	chr2pos2indices.clear();
	index2als.clear();
	index2pos.clear();
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		string chr = line[0];
		int pos = atoi(line[1].c_str());
		string ref = line[2];
		string alt = line[3];
		vector<double> tmpalpha;
		for (int i = 4; i < line.size(); i++) tmpalpha.push_back(log(atof(line[i].c_str())));
		if (index ==0) params->K = tmpalpha.size();
		assert(tmpalpha.size() == params->K);
		alpha.push_back(tmpalpha);
		index2pos.insert(make_pair(index, make_pair(chr, pos)));
		if (chr2pos2indices.find(chr) == chr2pos2indices.end()){
			map<int, vector<int> > tmp;
			//map<int, pair<string, string> > tmp2;
			chr2pos2indices.insert(make_pair(chr, tmp));
			//chr2pos2als.insert(make_pair(chr, tmp2));
		}
		if (chr2pos2indices[chr].find(pos) == chr2pos2indices[chr].end()){
			vector<int> indices;
			chr2pos2indices[chr].insert(make_pair(pos, indices));
		}
		chr2pos2indices[chr][pos].push_back(index);
		index2als.insert(make_pair(index, make_pair(ref, alt)));


		index++;
	}
	nsnp = index;
}

void Haplotypes::setthetafromfile(string infile){
	theta.clear();
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		string chr = line[0];
		int pos = atoi(line[1].c_str());
		string ref = line[2];
		string alt = line[3];
		vector<double> tmptheta;
		pair<string, int> knownchr = index2pos[index];
		assert(chr == knownchr.first);
		assert(pos == knownchr.second);
		for (int i = 4; i < line.size(); i++) tmptheta.push_back(atof(line[i].c_str()));
		assert(tmptheta.size() == params->K);
		theta.push_back(tmptheta);

		index++;
	}
	assert(nsnp == index);
}


void Haplotypes::setrratefromfile(string infile){
	rrate.clear();
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		assert(line.size()==5);
		string chr = line[0];
		int pos = atoi(line[1].c_str());
		string ref = line[2];
		string alt = line[3];
		double rr = atof(line[4].c_str());
		pair<string, int> knownchr = index2pos[index];
		assert(chr == knownchr.first);
		assert(pos == knownchr.second);
		rrate.push_back(rr);

		index++;
	}
	assert(nsnp == index);
}
void Haplotypes::read_haplotypes(string infile){
	d.clear();
	samples.clear();
	chr2pos2indices.clear();
	index2als.clear();
	index2pos.clear();
	cout << "Reading "<< infile << "\n";
	//using htslib
	bcf_srs_t *sr =  bcf_sr_init();
	bcf_sr_add_reader (sr, infile.c_str() );
	bcf_hdr_t *header = sr->readers->header;
	bcf1_t *line; //VCF line gets put in here
	int Nsamp=bcf_hdr_nsamples(header);
	for (int i = 0; i < Nsamp; i++){
		samples.push_back(header->samples[i]);
	}
	int index = 0;
	while(bcf_sr_next_line (sr)) { //loop through file
	   line =  bcf_sr_get_line(sr, 0);  //read a line
	   bcf_unpack(line, BCF_UN_ALL);
	   // to do: first check filters, make sure it's not monomorphic, is bi-allelic, etc.
	   // should be running on filtered 1000 Genomes data for now so not a huge priority
	   string chr =  bcf_hdr_id2name(header, line->rid);
	   int pos = line->pos+1;
	   string ref = line->d.allele[0];
	   string alt = line->d.allele[1];
	   int *gt = NULL;
	   int32_t n = 0;

	   int ngt = bcf_get_genotypes(header, line, &gt, &n);
	   if(index == 0) nhap = ngt;
	   // number of alleles should be 2x number of samples
	   if (ngt != Nsamp*params->autosomal_ploidy && !params->sexchrom){
		   cerr <<"at "<< chr << " "<< pos << " number of individuals and genotypes is not consistent\n";
		   exit(1);
	   }
	   vector<bool> alleles;
	   for (int i = 0; i < ngt; i++){
		   int32_t *ptr = gt+i;
		   int idx = bcf_gt_allele(ptr[0]);
		   bool al = false;
		   if (idx>=1) al = true;
		   alleles.push_back(al);
	   }
	   d.push_back(alleles);
	   index2pos.insert(make_pair(index, make_pair(chr, pos)));
	   if (chr2pos2indices.find(chr) == chr2pos2indices.end()){
		   map<int, vector<int> > tmp;
		   //map<int, pair<string, string> > tmp2;
		   chr2pos2indices.insert(make_pair(chr, tmp));
		   //chr2pos2als.insert(make_pair(chr, tmp2));
	   }
	   if (chr2pos2indices[chr].find(pos) == chr2pos2indices[chr].end()){
		   vector<int> indices;
		   chr2pos2indices[chr].insert(make_pair(pos, indices));
	   }
	   chr2pos2indices[chr][pos].push_back(index);
	   index2als.insert(make_pair(index, make_pair(ref, alt)));
	   index++;
	   if (index % 10000==0) {
		   cout << "\rRead "<< index <<" variants";
		   cout.flush();
	   }
	   free(gt);

	}
	nsnp = index;
	cout << "\nDone reading variants\n";
	cout.flush();
}


vector<pair<string, string> > Haplotypes::get_als(pair<string, int> info){
	vector<pair<string, string> > toreturn;
	if (chr2pos2indices.find(info.first) != chr2pos2indices.end() && chr2pos2indices[info.first].find(info.second) != chr2pos2indices[info.first].end() ){
		vector<int> indices = chr2pos2indices[info.first][info.second];
		for (vector<int>::iterator it = indices.begin(); it != indices.end(); it++){
			pair<string, string> tmp = index2als[*it];
			toreturn.push_back(tmp);
		}
		return(toreturn);
	}
	else{
		cerr << "ERROR: no such SNP chr"<< info.first << ":"<< info.second << "\n";
		exit(1);
	}
}

void Haplotypes::read_haplotypes_flatfile(string infile){
	d.clear();
	samples.clear();
	chr2pos2indices.clear();
	//make sure infile is gzipped
	string ext = infile.substr(infile.size()-3, 3);
	if (ext != ".gz"){
		std::cerr << infile << " is not gzipped (only .gz files accepted)\n";
		exit(1);
	}
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		string chr = line[0];
		int pos = atoi(line[1].c_str());
		if (index == 0) nhap = line.size()-2;
		vector<bool> alleles;
		for (int i = 2; i < line.size(); i++){
			int a = atoi(line[i].c_str());
			bool al = false;
			if (a>=1) al = true;
			alleles.push_back(al);
		}
		d.push_back(alleles);
		index2pos.insert(make_pair(index, make_pair(chr, pos)));
		index++;

	}
	nsnp = index;
}

void Haplotypes::read_params(string stem){
	alpha.clear();
	rrate.clear();
	theta.clear();

	read_alpha(stem+".alpha.gz");
	read_theta(stem+".theta.gz");
	read_rrate(stem+".rrate.gz");
	assert(alpha.size() == theta.size());
	assert(alpha.size() == rrate.size());
	assert(alpha[0].size() == theta[0].size());
	assert(alpha[0].size()==params->K);
}

void Haplotypes::read_alpha(string infile){
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		vector<double> tmpalpha;
		for (vector<string>::iterator it = line.begin(); it != line.end(); it++) tmpalpha.push_back(atof(it->c_str()));
		alpha.push_back(tmpalpha);
	}
}


void Haplotypes::read_theta(string infile){
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		vector<double> tmptheta;
		for (vector<string>::iterator it = line.begin(); it != line.end(); it++) tmptheta.push_back(atof(it->c_str()));
		theta.push_back(tmptheta);
	}
}

void Haplotypes::read_rrate(string infile){
	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}
		double tmp = atof(line[0].c_str());
		rrate.push_back(tmp);
	}
}
void Haplotypes::init_params(){
	alpha.clear();
	rrate.clear();
	theta.clear();
	holdfwd.clear();
	holdbwd.clear();
	eJ.clear();
	for (int i = 0; i < nsnp; i++){
		//cout << i << "\n";
		vector<double> tmpalpha;
		vector<double> tmptheta;
		vector<double> tmpfwd;
		vector<double> tmpbwd;
		vector<double> tmpJ;
		for (int j = 0; j < params->K ;j++){
			//cout << j << "\n";
			double tttheta[params->K];
			double ttalpha[params->K];
			tmpfwd.push_back(0);
			tmpbwd.push_back(0);
			tmpJ.push_back(-1000);
			for (int k = 0; k < params->K; k++) {
				//cout << k << "\n";
				ttalpha[k] = 1;
				//cout << "here\n";
				double rflat = gsl_ran_flat(r, 0.01, 0.99);
				//cout << rflat << "\n";
				tmptheta.push_back(rflat);
			}
			gsl_ran_dirichlet(r, params->K, ttalpha, tttheta);
			for (int k = 0; k < params->K; k++) tmpalpha.push_back(log(tttheta[k]));
		}

		rrate.push_back(0.001);
		alpha.push_back(tmpalpha);
		theta.push_back(tmptheta);
		holdfwd.push_back(tmpfwd);
		holdbwd.push_back(tmpbwd);
		eJ.push_back(tmpJ);
	}
}

void Haplotypes::print_params(){
	string routfile = params->outstem+".rout.gz";
	string aoutfile = params->outstem+".alphaout.gz";
	string toutfile = params->outstem+".thetaout.gz";
	//string ejoutfile = params->outstem+".Jout.gz";
	ogzstream rout(routfile.c_str());
	ogzstream aout(aoutfile.c_str());
	ogzstream tout(toutfile.c_str());
	//ogzstream jout(ejoutfile.c_str());
	for (int i = 0; i < nsnp; i++){



		map<int, pair<string, int> >::iterator info = index2pos.find(i);
		string ref = index2als[i].first;
		string alt = index2als[i].second;
		aout << info->second.first << "\t"<< info->second.second << "\t"<< ref << "\t"<< alt;
		rout << info->second.first << "\t"<< info->second.second << "\t"<< ref << "\t"<< alt;
		tout << info->second.first << "\t"<< info->second.second << "\t"<< ref << "\t"<< alt;
		rout << "\t"<< rrate.at(i)<< "\n";
		for (int j = 0 ; j < params->K; j++){
			aout << "\t"<< exp(alpha.at(i).at(j));
			tout << "\t"<< theta.at(i).at(j) << " ";
			//jout << exp(eJ.at(i).at(j)) << " ";
		}

		aout << "\n";
		tout << "\n";
		//jout << "\n";
	}

}


double Haplotypes::ltransition(int snpindex, int from, int to, bool onlyswitch = false){
	//log probability of going from state [from] to state [to] in snpindex-1 to snpindex
	double toreturn;
	//initial state is only 0
	if (snpindex == 0 && from == 0) toreturn = alpha[0][to];
	//cannot start from other states (approximate as exp(-100000) = 0)
	else if (snpindex==0) toreturn = -1000000;
	else{
		// probability of ending up in state [to] if there is a state switch
		//toreturn =  log(alpha[snpindex][to])+log(1-exp(-rrate[snpindex]));
		toreturn = alpha[snpindex][to] + log(rrate[snpindex]);
		// probability of ending up in state [to] if there is not a state switch
		//if (!onlyswitch and from == to) toreturn = sumlog(toreturn, -rrate[snpindex]);
		if (from == to and !onlyswitch) toreturn = sumlog(toreturn, log(1-rrate[snpindex]));
	}
	return toreturn;
}


double Haplotypes::lemit(int snpindex, int state, bool val){
	double toreturn;
	if (val) toreturn = log(theta[snpindex][state]);
	else toreturn = log(1-theta[snpindex][state]);
	//cout << toreturn << " "<< snpindex << " "<< state << " "<< val << " "<< theta[snpindex][state]<< "\n";
	return toreturn;
}

double Haplotypes::fwdlogp(int hapi){
	double toreturn;
	assert(nsnp >1);
	assert(hapi <nhap);
	for (int i = 0; i < params->K ;i++){
		double eprob = lemit(0, i, d[0][hapi]);
		double tmpalpha = alpha[0][i];
		holdfwd[0][i] = eprob+tmpalpha;
	}
	for (int i = 1; i< nsnp; i++){
		for (int j = 0; j < params->K; j++){

			double lsum;
			//log emission at position i from state j
			double tmp = lemit(i, j, d[i][hapi]);

			// probability of ending up at state j if previously in k

			for (int k = 0; k < params->K; k++){
				// previous forward p at k
				double prevlogfwd = holdfwd[i-1][k];

				//transition to j from k if there is a recombination
				double logtransition = ltransition(i, k, j);

				double fwdtransition = prevlogfwd+logtransition;
				//cout << "position "<< i << " transition from "<< k << " to "<< j << " :"<< exp(logtransition) <<" "<< prevlogfwd << "\n";
				if (k == 0) lsum = fwdtransition;
				else lsum = sumlog(lsum, fwdtransition);
			}

			//cout << i << " "<< j << " "<< tmp << " "<< lsum << "\n";
			holdfwd[i][j] = tmp+lsum;
		}
	}
	toreturn = holdfwd[nsnp-1][0];
	for (int i = 1; i < params->K; i++) toreturn = sumlog(toreturn, holdfwd[nsnp-1][i]);
	return toreturn;
}

double Haplotypes::fwdlogp_linear(int hapi){
	double toreturn;
	assert(nsnp >1);
	assert(hapi <nhap);
	//initial states
	for (int i = 0; i < params->K ;i++){
		double eprob = lemit(0, i, d[0][hapi]);
		double tmpalpha = alpha[0][i];
		holdfwd[0][i] = eprob+tmpalpha;
	}
	for (int i = 1; i< nsnp; i++){
		//sum over all previous forward variables
		double prevsum;

		for (int j = 0; j < params->K; j++){
			if (j == 0) prevsum = holdfwd[i-1][j];
			else prevsum = sumlog(prevsum, holdfwd[i-1][j]);
		}
		double rr = rrate[i];
		for (int j = 0; j < params->K; j++){
			//log emission at position i from state j
			double eprob = lemit(i, j, d[i][hapi]);
			double secondterm = log(rr)+alpha[i][j]+prevsum;

			double firstterm = holdfwd[i-1][j]+ log(1-rr);
			double termsum = sumlog(firstterm, secondterm);
			holdfwd[i][j] = termsum+eprob;
		}
	}
	toreturn = holdfwd[nsnp-1][0];
	for (int i = 1; i < params->K; i++) toreturn = sumlog(toreturn, holdfwd[nsnp-1][i]);
	if (isnan(toreturn)) {
		print_fwd();
		print_params();
		cout << hapi << "\n";
		exit(1);
	}
	return toreturn;
}


double Haplotypes::bwdlogp(int hapi){
	double toreturn;
	assert(nsnp >1);
	assert(hapi <nhap);
	//initialize B(nsnp) as log (1)
	for (int i = 0; i < params->K; i++) holdbwd[nsnp-1][i] = 0;
	for (int i = nsnp-2; i>=0; i--){
		for (int j = 0; j < params->K; j++){
			double lsum;
			for (int k = 0; k < params->K; k++){
				// emission at state i+1 from state k
				double tmp = lemit(i+1, k, d[i+1][hapi]);

				// bwd at k
				double prevlogbwd = holdbwd[i+1][k];

				// transition from j to k
				double logtransition = ltransition(i+1, j, k);

				//sum
				double bwdtransition = prevlogbwd+logtransition+tmp;
				if (k == 0) lsum = bwdtransition;
				else lsum = sumlog(lsum, bwdtransition);
			}
			holdbwd[i][j] = lsum;
		}
	}


	//emission at position 0 from state 0
	double tmp = lemit(0, 0, d[0][hapi]);

	//transition to state 0
	double tmpa = alpha[0][0];
	toreturn = holdbwd[0][0]+ tmp +tmpa;
	for (int i = 1; i < params->K; i++) {
		double tmp2 = lemit(0, i, d[0][hapi]);
		tmp2+= alpha[0][i];
		toreturn = sumlog(toreturn, holdbwd[0][i] +tmp2);
	}

	return toreturn;
}


double Haplotypes::bwdlogp_linear(int hapi){
	double toreturn;
	assert(nsnp >1);
	assert(hapi <nhap);
	//initialize B(nsnp) as log (1)
	for (int i = 0; i < params->K; i++) holdbwd[nsnp-1][i] = 0;
	for (int i = nsnp-2; i>=0; i--){
		double sumbwd;
		double rr = rrate[i+1];

		for (int j = 0; j < params->K; j++){
			double emit = lemit(i+1, j, d[i+1][hapi]);
			double prevlogbwd = holdbwd[i+1][j];
			double land = alpha[i+1][j];
			double toadd = emit+prevlogbwd+land;
			if (j == 0) sumbwd = toadd;
			else sumbwd = sumlog(sumbwd, toadd);
		}
		for (int j = 0; j < params->K; j++){

			double emitstay = lemit(i+1, j, d[i+1][hapi]);
			double bwdstay =  holdbwd[i+1][j];
			double term1 = log(1-rr)+emitstay+bwdstay;
			double term2 = log(rr)+ sumbwd;
			double toadd = sumlog(term1, term2);
			holdbwd[i][j] = toadd;
		}
	}


	//emission at position 0 from state 0
	double tmp = lemit(0, 0, d[0][hapi]);

	//transition to state 0
	double tmpa = alpha[0][0];
	toreturn = holdbwd[0][0]+ tmp +tmpa;
	//cout << toreturn << "\n";
	for (int i = 1; i < params->K; i++) {
		double tmp2 = lemit(0, i, d[0][hapi]);
		tmp2+= alpha[0][i] +holdbwd[0][i];
		//cout << tmp2 << "\n";
		toreturn = sumlog(toreturn, tmp2);
	}

	return toreturn;
}

double Haplotypes::llk(){
	assert(nhap >0);
	double toreturn = fwdlogp_linear(0);
	for (int i = 1; i < nhap; i++){
		toreturn += fwdlogp_linear(i);
	}
	return toreturn;
}

void Haplotypes::print_fwd(){
	string routfile = params->outstem+".fwd.gz";
	ogzstream out(routfile.c_str());
	for(vector<vector<double> >::iterator it = holdfwd.begin(); it != holdfwd.end(); it++){
		for(vector<double>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			out << *it2 << " ";
		}
		out << "\n";
	}
}


void Haplotypes::print_bwd(){
	string routfile = params->outstem+".bwd.gz";
	ogzstream out(routfile.c_str());
	for(vector<vector<double> >::iterator it = holdbwd.begin(); it != holdbwd.end(); it++){
		for(vector<double>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			out << *it2 << " ";
		}
		out << "\n";
	}
}

void Haplotypes::print_d(int hapi){
	string routfile = params->outstem+".d.gz";
	ogzstream out(routfile.c_str());
	for (int i = 0; i < nsnp; i++){
		out << d[i][hapi]<< "\n";
	}
}
double Haplotypes::sumlog(double logx, double logy){
        if (logx > logy) return logx + log(1 + exp(logy-logx));
        else return logy + log(1 + exp(logx-logy));
}

void Haplotypes::update_theta(){
	vector<vector<float> > tmptheta, tmpotheta;
	for (int i = 0; i < nsnp; i++){
		vector<float> tmp;
		vector<float> tmpo;
		for (int j = 0; j < params->K; j++){
			tmp.push_back(-10000);
			tmpo.push_back(-10000);
		}
		tmptheta.push_back(tmp);
		tmpotheta.push_back(tmpo);
	}
	for (int i = 0; i < nhap; i++){
		//this sets the forward and backward variables in holdfwd and holdbwd
		double llkfwd = fwdlogp(i);
		double llkbwd = bwdlogp(i);
		for (int j = 0; j < nsnp; j++){
			if (d[j][i]){
				for (int k = 0; k < params->K; k++){
					double lfwd_jk = holdfwd[j][k];
					double lbwd_jk = holdbwd[j][k];
					double toadd =  lfwd_jk +lbwd_jk - llkfwd;
					tmptheta[j][k] = sumlog(tmptheta[j][k], toadd);
				}

			}
			else{
				for (int k = 0; k < params->K; k++){
					double lfwd_jk = holdfwd[j][k];
					double lbwd_jk = holdbwd[j][k];
					double toadd =  lfwd_jk +lbwd_jk - llkfwd;
					tmpotheta[j][k] = sumlog(tmpotheta[j][k], toadd);
				}
			}
		}
	}
	for (int i = 0; i < nsnp ; i++){
		for (int j = 0; j < params->K ; j++){
			theta[i][j] = exp(tmptheta[i][j])/( exp(tmptheta[i][j]) + exp(tmpotheta[i][j])  );
		}
	}
}

void Haplotypes::update_J_and_theta(){

	//initialize J (counts of jumps between positions i-1 and i that go to state j
	for (int i = 0; i< nsnp; i++){
		for (int j = 0; j < params->K; j++){
			eJ[i][j] = -10000;
		}
	}

	//initialize tmptheta, tmpothera (counts of alleles in position i that are from state j
	vector<vector<float> > tmptheta, tmpotheta;
	for (int i = 0; i < nsnp; i++){
		vector<float> tmp;
		vector<float> tmpo;
		for (int j = 0; j < params->K; j++){
			tmp.push_back(-10000);
			tmpo.push_back(-10000);
		}
		tmptheta.push_back(tmp);
		tmpotheta.push_back(tmpo);
	}
	for (int i = 0; i < nhap ; i++){
		cout << "\rcomputing contribution of haplotype "<< i+1 << "/"<< nhap; cout.flush();
		//for each haplotypes
		//this sets the forward and backward variables in holdfwd and holdbwd
		double llkfwd = fwdlogp_linear(i);
		//print_fwd();
		double llkbwd = bwdlogp_linear(i);
		//print_bwd();
		//cout << std::setprecision(8)<< "\n"<< llkfwd << " "<< llkbwd << " "<< fwdlogp(i)<< " "<< bwdlogp(i)<< "\n";
		//assert( (llkfwd-llkbwd)*(llkfwd-llkbwd) < 0.00000001);
		if (i ==0 ) current_llk = llkfwd;
		else current_llk+=llkfwd;
		#pragma omp parallel for
		for (int j = 0; j < nsnp; j++){

			if (d[j][i]){
				for (int k = 0; k < params->K; k++){
					double lfwd_jk = holdfwd[j][k];
					double lbwd_jk = holdbwd[j][k];
					double toadd =  lfwd_jk +lbwd_jk - llkfwd;
					tmptheta[j][k] = sumlog(tmptheta[j][k], toadd);
				}

			}
			else{
				for (int k = 0; k < params->K; k++){
					double lfwd_jk = holdfwd[j][k];
					double lbwd_jk = holdbwd[j][k];
					double toadd =  lfwd_jk +lbwd_jk - llkfwd;
					tmpotheta[j][k] = sumlog(tmpotheta[j][k], toadd);
				}
			}
			//at each snp, calculate E[switch]_k

			//sum all previous forward variables
			double lsum;
			if (j == 0) lsum = 0;
			else{
				for (int k = 0; k < params->K; k++){
					if (k == 0) lsum = holdfwd[j-1][k];
					else lsum = sumlog(lsum, holdfwd[j-1][k]);
				}
			}
			double rr = log(rrate[j]);
			if (j ==0 )rr = 0;
			for (int k =0; k < params->K; k++){

				double tmpa = alpha[j][k];
				double le = lemit(j, k, d[j][i]);
				double lbwd = holdbwd[j][k];
				double toadd = lsum + lbwd+le +tmpa + rr - llkfwd;
				/*
				if (toadd > 0) {
					print_params();
					print_fwd();
					print_bwd();
					cout << "\n"<< i << " "<< j << " "<< k << " "<< le << " "<< lbwd << " "<< lsum << " "<< llkfwd << " "<< toadd <<"\n";
					assert(toadd < 1e-10);
					exit(1);
				}*/

				eJ[j][k] = sumlog(eJ[j][k], toadd);
			}
		}
	}

	//set theta to updated parameters
	#pragma omp parallel for
	for (int i = 0; i < nsnp ; i++){
		for (int j = 0; j < params->K ; j++){
			double toput = exp(tmptheta[i][j])/( exp(tmptheta[i][j]) + exp(tmpotheta[i][j])  );
			if (toput < params->epsilon) toput = params->epsilon;
			if (toput > 1-params->epsilon) toput = 1-params->epsilon;
			theta[i][j] =toput;
		}
	}

}
void Haplotypes::update_J(){
	for (int i = 0; i< nsnp; i++){
		vector<float> tmp;
		for (int j = 0; j < params->K; j++){
			eJ[i][j] = -10000;
		}
	}
	for (int i = 0; i < nhap ; i++){
		//this sets the forward and backward variables in holdfwd and holdbwd
		double llkfwd = fwdlogp(i);
		double llkbwd = bwdlogp(i);
		//for (int j = 0; j < params->K; j++){
		//	eJ[0][j] = sumlog(eJ[0][j], log(alpha[0][j]));
		//}
		for (int j = 0; j < nsnp; j++){
			//cout << j << " j\n";
			//at each snp, calculate E[switch]_k

			for (int k =0; k < params->K; k++){

				double lsum;
				//if the first SNP, just probability of starting there
				if (j == 0)	lsum = alpha[0][k];
				else{
				// sum over all previous states
					for (int l = 0; l< params->K; l++){
					//probability of being in state l at previous snp
						double lfwd = holdfwd[j-1][l];

						//transition from l to k from a jump
						double lt = ltransition(j, l, k, true);
						//cout << "transition at "<< j << " from "<< l << " to "<< k << ": "<< lt << "\n";
						if (l == 0) lsum = lfwd+lt;
						else lsum = sumlog(lsum, lfwd +lt);
					}
				}

				double le = lemit(j, k, d[j][i]);
				double lbwd = holdbwd[j][k];

				double toadd = lsum + lbwd+le - llkfwd;
				//cout << le << " "<< lbwd << " "<< lsum << " "<< llkfwd << " "<< toadd <<"\n";
				eJ[j][k] = sumlog(eJ[j][k], toadd);
			}
		}
	}
}

void Haplotypes::update_rrate(){
	for(int i = 0; i< nsnp; i++){
		if (i == 0) rrate[i] = 1;
		else{
			double num = 0;
			double denom = (float) nhap;
			for(int j = 0; j < params->K ; j++) {
				num+= exp(eJ[i][j]);
			}

			rrate[i] = num/denom;
		}
	}
}

void Haplotypes::update_alpha_and_rrate(){
	//alpha and rrate are updated only with expected jump counts
	#pragma omp parallel for
	for(int i = 0; i< nsnp; i++){
		if (i == 0) rrate[i] = 1;
		else{
			double num = 0;
			double denom = (float) nhap;
			for(int j = 0; j < params->K ; j++) {
				num+= exp(eJ[i][j]);
			}
			double tmprrate = num/denom;
			if (tmprrate < params->epsilon) tmprrate = params->epsilon;
			if (tmprrate > 1) cout << i << num << " "<< denom << "\n";
			rrate[i] = tmprrate;
		}

		//just duplicating update_alpha code, could make more efficient by using num from above
		double alphadenom = 0;
		for(int j = 0; j < params->K; j++)	alphadenom += exp(eJ[i][j]);
		for (int j = 0; j < params->K; j++) {
			double tmpalpha =  exp(eJ[i][j])/alphadenom;
			if (tmpalpha < params->epsilon) tmpalpha = params->epsilon;
			if (tmpalpha > 1-params->epsilon) tmpalpha = 1-params->epsilon;
			alpha[i][j] = log(tmpalpha);
		}
	}
}

void Haplotypes::update_alpha(){
	for(int i = 0; i< nsnp; i++){
		double denom = 0;
		for(int j = 0; j < params->K; j++)	denom += exp(eJ[i][j]);
		for (int j = 0; j < params->K; j++) alpha[i][j] = log(exp(eJ[i][j])/denom);
	}
}

void Haplotypes::EM_step(){
	//expected jumps depend on parameters, as do ML allele frequencies
	update_J_and_theta();
	//ML alpha and rrate depend only on expected jumps
	update_alpha_and_rrate();

	//update_J();
	//update_theta();
	//update_alpha();
	//update_rrate();
}
void Haplotypes::EM(int count){
	for (int i = 0 ; i< count ; i++) {
		cout << "EM step "<< i << "\n"; cout.flush();
		EM_step();
		cout << "\nEM step "<< i << " completed, llk was "<< current_llk << "\n"; cout.flush();
	}
}
