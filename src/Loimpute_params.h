/*
 * Loimpute_params.h
 *
 *  Created on: Dec 18, 2016
 *      Author: jkpickrell
 */

#ifndef LOIMPUTE_PARAMS_H_
#define LOIMPUTE_PARAMS_H_

#define __STDC_LIMIT_MACROS
#include <limits>
#include <htslib/synced_bcf_reader.h>
#include <htslib/hts.h>
#include <string>
#include <map>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <stack>
#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ostream>
#include <cstdlib>
#include "CmdLine.h"
#include "gzstream.h"
#include <sys/stat.h>
#include <iomanip>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <boost/algorithm/string.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif

using std::string;
using std::vector;
using std::list;
using std::stack;
using std::map;
using std::set;
using std::multiset;
using std::cout;
using std::cin;
using std::endl;
using std::ostream;
using std::ofstream;
using std::stringstream;
using std::pair;
using std::iterator;
using std::pair;
using std::make_pair;
using std::fstream;
using std::ifstream;

class Loimpute_params{
public:
	Loimpute_params();
	string hapfile, pileupfile, outstem;
	int seed, K;
	bool sexchrom, readpileup;
	int autosomal_ploidy, niter, nthread, maxreads, minoverlap;
	int minprint, maxprint;
	bool printrange;
	double epsilon, erate; //if MLE of a parameter is 0, set it to epsilon.
	bool read_vcf, read_params, imputationmode;
	string paramstem, samplename;
	void print();
};

#endif /* LOIMPUTE_PARAMS_H_ */
