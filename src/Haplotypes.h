/*
 * haplotype.h
 *
 *  Created on: Dec 18, 2016
 *      Author: jkpickrell
 */

#ifndef HAPLOTYPE_H_
#define HAPLOTYPE_H_

#include "Loimpute_params.h"

class Haplotypes{
public:
	Haplotypes();
	Haplotypes(Loimpute_params *, gsl_rng *);
	Loimpute_params *params;
	int nhap, nsnp;
	gsl_rng *r;
	vector<string> samples;
	map<string, int> pop2id;
	map<int, string> id2pop;
	map<string, map<int, vector<int> > > chr2pos2indices;
	map<int, pair<string, string > > index2als;
	map<int, pair<string, int> > index2pos;
	vector<pair<string, string> > get_als(pair<string, int>);
	// model parameters
	vector<vector<double> > alpha; //stored in log
	vector<double> rrate;// *not* log
	vector< vector<double> > theta; //*not* log
	vector<vector<double> > holdfwd; //log
	vector<vector<double> > holdbwd;// log
	vector<vector<double> > eJ; //log
	//data
	vector<vector<bool> > d;

	//
	void read_haplotypes(string);
	void read_haplotypes_flatfile(string); //for testing
	void read_params(string); //for testing
	void read_alpha(string);
	void read_theta(string);
	void read_rrate(string);
	void init_params();
	void print_params();
	double fwdlogp(int);
	double bwdlogp(int);
	double fwdlogp_linear(int); //fwd and backward algorithms that are linear in K
	double bwdlogp_linear(int);
	double llk();
	double ltransition(int, int, int, bool);
	double lemit(int, int, bool);
	void print_fwd();
	void print_bwd();
	void print_d(int);
	void update_params();
	void update_theta();
	void update_rrate();
	void update_alpha();
	void update_J();
	void update_J_and_theta();
	void update_alpha_and_rrate();
	void EM_step();
	void EM(int);
	void setmodelfromfiles(string); // read model parameters from files
	void setalphafromfile(string);
	void setrratefromfile(string);
	void setthetafromfile(string);
	double current_llk;

	//helper
	double sumlog(double, double);
};

#endif /* HAPLOTYPE_H_ */
