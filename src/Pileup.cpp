/*
 * Pileup.cpp
 *
 *  Created on: Dec 27, 2016
 *      Author: jkpickrell
 */

#include "Pileup.h"

Pileup::Pileup(){

}

Pileup::Pileup(Loimpute_params * params, Haplotypes * htmp){
	p = params;
	h = htmp;
	read_infile(p->pileupfile);
	cout << "Number of overlapping variants: " << nsnp << "\n";
	if (nsnp < p->minoverlap){
		cerr <<"ERROR: number of overlapping variants less than the minimum of "<< p->minoverlap << "\n";
		exit(1);
	}
	combine_w_haplotypes();
	init();
}

void Pileup::print(){
	for (int i = 0; i < h->nsnp; i++){
		cout << info_wmissing[i].first << " "<< info_wmissing[i].second;
		for (int j = 0; j < d_wmissing[i].size(); j++){
			cout << " "<< d_wmissing[i][j];
		}
		cout << "\n";
	}
}

void Pileup::combine_w_haplotypes(){
	d_wmissing.clear();
	info_wmissing.clear();
	for (int i = 0; i < h->nsnp; i++){
		pair<string, int> hinfo = h->index2pos[i];
		string chr = hinfo.first;
		int pos = hinfo.second;
		pair<string, string> refals = h->index2als[i];
		if (chr2pos2index.find(chr) != chr2pos2index.end() and chr2pos2index[chr].find(pos) != chr2pos2index[chr].end() and als[chr2pos2index[chr][pos]] == refals){
			int index = chr2pos2index[chr][pos];

			d_wmissing.push_back(d[index]);
			info_wmissing.push_back(info[index]);
		}
		else{
			vector<bool> tmp;
			d_wmissing.push_back(tmp);
			info_wmissing.push_back(hinfo);
		}
	}
}

void Pileup::init(){
	holdfwd.clear(); holdbwd.clear();
	for (int i = 0; i < h->nsnp; i++){
		vector<double> tmp, tmp2;
		for (int j = 0; j < p->K; j++){
			for (int k = 0; k < p->K; k++){
				tmp.push_back(0);
				tmp2.push_back(0);
			}
		}
		holdfwd.push_back(tmp);
		holdbwd.push_back(tmp2);
	}
}

double Pileup::lemit(int snpi, int z1, int z2){
	//snp index i
	//states z1 and z2

	double toreturn;
	//if (d_wmissing[snpi].size()==0) return 0;
	double lp0 = lemit_g(snpi, z1, z2, 0) + readp_giveng(snpi, 0);
	double lp1 = lemit_g(snpi, z1, z2, 1) + readp_giveng(snpi, 1);
	double lp2 = lemit_g(snpi, z1, z2, 2) + readp_giveng(snpi, 2);
	toreturn = h->sumlog(lp0, lp1);
	toreturn = h->sumlog(toreturn, lp2);
	//if (snpi == 133) cout << snpi << " "<< z1 << " "<< z2 << " "<< lp0 << " "<< lp1 << " "<< lp2 << " "<< toreturn << " "<< readp_giveng(snpi, 0) << " "<< readp_giveng(snpi, 1)<< " "<< readp_giveng(snpi, 2)<< " e\n";
		/*
	for (int i =0; i < d_wmissing[snpi].size(); i++){
		bool allele = d_wmissing[snpi][i];

		double t
		if (allele){
			toadd = 0.5* h->theta[snpi][z1]+ 0.5 * h->theta[snpi][z2];
		}
		else{
			toadd = 0.5* (1-h->theta[snpi][z1])+ 0.5 * (1-h->theta[snpi][z2]);
		}
		if (i == 0) toreturn = log(toadd);

		else toreturn+=  log(toadd);
	}
	*/
	return toreturn;
}


double Pileup::setfwd(bool null){
	//cout << "set fwd\n"; cout.flush();
	// set forward variables
	double toreturn;
	assert(d_wmissing.size() == h->nsnp);
	//initialize
	int index =0;
	for (int i = 0; i < p->K; i++){
		for (int j = 0; j < p->K; j++){
			//cout << index << "\n";
			double le = lemit(0, i, j);
			double lalpha = h->alpha[0][i]+ h->alpha[0][j];
			holdfwd[0][index] = le+lalpha;
			index++;
		}
	}
	//recursion
	for (int i = 1; i < h->nsnp; i++){
		//cout << i << "\n"; cout.flush();
		double rr = h->rrate[i];
		vector<double> fwdsums1(p->K, -1000000.0);
		vector<double> fwdsums2(p->K, -1000000.0);
		double allfwdsum = -1000000.0;
		index = 0;
		for (int j = 0; j < p->K; j++){
			for (int k = 0; k < p->K; k++){
				double prevfwd = holdfwd[i-1][index];
				allfwdsum =  h->sumlog(allfwdsum, prevfwd);
				fwdsums1[j] = h->sumlog(fwdsums1[j], prevfwd);
				fwdsums2[k] = h->sumlog(fwdsums2[k], prevfwd);
				index++;
			}
		}
		//cout << allfwdsum << " "<< i << " allfwdsum\n";
		index = 0;

		for (int j = 0; j < p->K; j++){
			for (int k = 0; k < p->K; k++){
				double le = lemit(i, j, k);
				if (null) le = 0;
				double term1= holdfwd[i-1][index] + 2*log(1-rr);
				double term2 = log(1-rr)+ log(rr)+ h->alpha[i][k] + fwdsums1[j];
				double term3 = log(1-rr)+ log(rr)+ h->alpha[i][j] + fwdsums2[k];
				double term4 = 2*log(rr)+ h->alpha[i][j] + h->alpha[i][k]+ allfwdsum;
				double toadd = h->sumlog(term1, term2);
				toadd = h->sumlog(toadd, term3);
				toadd = h->sumlog(toadd, term4);
				//cout << i << " "<< j << " "<< k << " "<< le << " "<< term1 << " "<< term2 << " "<< term3 << " "<< term4 << " "<<toadd << " fwd\n";
				holdfwd[i][index] = toadd+le;
				index++;
			}
		}

	}
	//termination
	index = 0;
	for (int j = 0; j < p->K; j++){
		for (int k = 0; k < p->K; k++){
			if (index == 0) toreturn = holdfwd[h->nsnp-1][index];
			else toreturn = h->sumlog(holdfwd[h->nsnp-1][index], toreturn);
			index++;
		}
	}

	return toreturn;
}

void Pileup::print_fwd(){
	string routfile = p->outstem+".gt-fwd.gz";
	ogzstream out(routfile.c_str());
	for(vector<vector<double> >::iterator it = holdfwd.begin(); it != holdfwd.end(); it++){
		for(vector<double>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			out << *it2 << " ";
		}
		out << "\n";
	}

}

void Pileup::print_bwd(){
	string routfile = p->outstem+".gt-bwd.gz";
	ogzstream out(routfile.c_str());
	for(vector<vector<double> >::iterator it = holdbwd.begin(); it != holdbwd.end(); it++){
		for(vector<double>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			out << *it2 << " ";
		}
		out << "\n";
	}
}

void Pileup::write_imputed_vcf(){
	string outfile = p->outstem+".vcf.gz";
	htsFile *fp    = hts_open(outfile.c_str(),"wz");
	bcf_hdr_t *hdr = bcf_hdr_init("w");
	bcf1_t *rec    = bcf_init1();

				// Create VCF header
	kstring_t str = {0,0,0};
	bcf_hdr_append(hdr, "##source=loimpute");
	//bcf_hdr_append(hdr, "##reference=file:///seq/references/1000GenomesPilot-NCBI36.fasta");
	for (map<string, map<int, vector<int> > >::iterator it = h->chr2pos2indices.begin(); it != h->chr2pos2indices.end(); it++) {
		string tmpstr = "##contig=<ID="+it->first+">";
		bcf_hdr_append(hdr, tmpstr.c_str());
	}
	bcf_hdr_append(hdr, "##phasing=unphased");
	bcf_hdr_append(hdr, "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">");
	bcf_hdr_append(hdr, "##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Genotype Likelihoods\">");
	bcf_hdr_append(hdr, "##FORMAT=<ID=RC,Number=1,Type=Integer,Description=\"Count of reads with REF allele\">");
	bcf_hdr_append(hdr, "##FORMAT=<ID=AC,Number=1,Type=Integer,Description=\"Count of reads with ALT allele\">");
	bcf_hdr_add_sample(hdr, p->samplename.c_str());
	bcf_hdr_add_sample(hdr, NULL);      // to update internal structures
	bcf_hdr_write(fp, hdr);

	for (int i = 0; i < h->nsnp; i++){
		// Add a record
		// 20     14370   rs6054257 G      A       29   PASS   NS=3;DP=14;AF=0.5;DB;H2           GT:GQ:DP:HQ 0|0:48:1:51,51 1|0:48:8:51,51 1/1:43:5:.,.
		// .. CHROM
		bcf_clear1(rec);
		pair<string, int> info = info_wmissing[i];

		string chr = info.first;
		int pos = info.second;
		if (p->printrange){
			if (pos < p->minprint or pos > p->maxprint) continue;
		}
		stringstream ss;
		ss << pos;
		string posstr = ss.str();
		rec->rid = bcf_hdr_name2id(hdr, chr.c_str());
		pair<string, string> alleles = h->index2als[i];
		string aa = alleles.first+","+alleles.second;
		// .. POS
		rec->pos = pos-1; //0-based
		// .. ID
		string tmpid = chr+":"+posstr+":"+alleles.first+":"+alleles.second;
		bcf_update_id(hdr, rec, tmpid.c_str());
		// .. REF and ALT
		bcf_update_alleles_str(hdr, rec, aa.c_str());
		// .. QUAL
		//rec->qual = 1;
		// .. FILTER
		//  int32_t tmpi = bcf_hdr_id2int(hdr, BCF_DT_ID, "PASS");
		//  bcf_update_filter(hdr, rec, &tmpi, 1);
		// .. INFO
		/*
		   tmpi = 3;
		   	   bcf_update_info_int32(hdr, rec, "NS", &tmpi, 1);
		   tmpi	 = 14;
		   bcf_u	pdate_info_int32(hdr, rec, "DP", &tmpi, 1);
		   float tmpf = 0.5;
		   bcf_update_info_float(hdr, rec, "AF", &tmpf, 1);
		   bcf_update_info_flag(hdr, rec, "DB", NULL, 1);
		   bcf_update_info_flag(hdr, rec, "H2", NULL, 1);
		   */
		   // .. FORMAT
		vector<double> lks = gt_lk[i];
		int maxi = 0;
		double max = lks[0];
		if (lks[1] > lks[0]){
			maxi = 1;
			max = lks[1];
		}
		if (lks[2]>max){
			maxi = 2;
			max = lks[2];
		}
		//genotypes
		int32_t *tmpia = (int*)malloc(bcf_hdr_nsamples(hdr)*2*sizeof(int));

		if(maxi == 0){
			tmpia[0] = bcf_gt_unphased(0);
			tmpia[1] = bcf_gt_unphased(0);
		}
		else if(maxi == 1){
			tmpia[0] = bcf_gt_unphased(0);
			tmpia[1] = bcf_gt_unphased(1);
		}
		else if(maxi == 2){
			tmpia[0] = bcf_gt_unphased(1);
			tmpia[1] = bcf_gt_unphased(1);
		}
		else{
			cerr << "No genotype? for "<< i << "\n";
			exit(1);
		}

		bcf_update_genotypes(hdr, rec, tmpia, bcf_hdr_nsamples(hdr)*2);
		// sequencing depth
		int rc = 0;
		int ac = 0;
		for (vector<bool>::iterator it = d_wmissing[i].begin(); it != d_wmissing[i].end(); it++){
			if (*it) ac++;
			else rc++;
		}
		tmpia[0] = rc;

		bcf_update_format_int32(hdr, rec, "RC", tmpia, bcf_hdr_nsamples(hdr));
		tmpia[0] = ac;

		bcf_update_format_int32(hdr, rec, "AC", tmpia, bcf_hdr_nsamples(hdr));

		// likelihoods

		float *tmpfa = (float*)malloc(bcf_hdr_nsamples(hdr)*3*sizeof(float));
		tmpfa[0] = log10(exp(lks[0]));
		tmpfa[1] =  log10(exp(lks[1]));
		tmpfa[2] =  log10(exp(lks[2]));

		bcf_update_format_float(hdr, rec, "GL", tmpfa,bcf_hdr_nsamples(hdr)*3);

		bcf_write1(fp, hdr, rec);

		// 20     1110696 . A      G,T     67   .   NS=2;DP=10;AF=0.333,.;AA=T;DB GT 2 1   ./.
		bcf_clear1(rec);
		free(tmpia);
		free(tmpfa);


	   }
	   // Clean
	   free(str.s);
	   bcf_destroy1(rec);
	   bcf_hdr_destroy(hdr);
	   int ret;
	   if ( (ret=hts_close(fp)) )
	   {
		   fprintf(stderr,"hts_close(): non-zero status %d\n",ret);
		   exit(ret);
	   }
}

double Pileup::kl(int snpi){
	double toreturn;
	assert(gt_lk.size() > snpi);
	vector<double> lks = gt_lk[snpi];
	vector<double> null = gt_lk_null[snpi];

	//cout << "kl "<< exp(null[0])<< " "<< exp(null[1])<< " "<<exp(null[2])<< "\n";
	toreturn = exp(lks[0]) * (lks[0]-null[0]);
	toreturn += exp(lks[1]) * (lks[1]-null[1]);
	toreturn += exp(lks[2]) * (lks[2]-null[2]);
	//cout << toreturn << "\n";
	return toreturn;
}

void Pileup::impute(){
	//set_null();
	gt_lk.clear();
	llk = setfwd();
	double bwdlk = setbwd();
	for (int i = 0; i < h->nsnp; i++){
		vector<double> lk = impute(i);
		//cout << i << " "<< exp(lk[0])<< " "<< exp(lk[1])<< " "<< exp(lk[2])<<" "<< d_wmissing[i].size()<<"\n";

		gt_lk.push_back(lk);
		//kl(i);
	}
}


void Pileup::set_null(){
	gt_lk_null.clear();
	llk = setfwd(true);
	double bwdlk = setbwd(true);
	for (int i = 0; i < h->nsnp; i++){
		vector<double> lk = impute(i, true);
		//cout << i << " "<< exp(lk[0])<< " "<< exp(lk[1])<< " "<< exp(lk[2])<<" "<< d_wmissing[i].size()<<"\n";

		gt_lk_null.push_back(lk);
	}
}



void Pileup::impute_state(){
	state_lk.clear();
	llk = setfwd();
	double bwdlk = setbwd();
	for (int i = 0; i < h->nsnp; i++){
		vector<double> tmp;
		int index  =0;
		for (int j = 0; j < p->K; j++){
			for (int k = j; k < p->K; k++){
				double prob = holdfwd[i][index]+holdbwd[i][index]-llk;
				tmp.push_back(prob);
				index++;
			}
		}
		state_lk.push_back(tmp);
		for (vector<double>::iterator it = tmp.begin(); it != tmp.end(); it++) cout << exp(*it) <<" ";
		cout << "\n";
	}
}
double Pileup::readp_giveng(int snpi, int g){
	double toreturn;
	if (d_wmissing[snpi].size()==0) return 0.0;
	for (int i =0; i < d_wmissing[snpi].size(); i++){
		bool allele = d_wmissing[snpi][i];
		double toadd;
		if (allele){
			if (g==2) toadd = 0.0;
			else if (g==1) toadd = log(0.5);
			else if (g==0) toadd = log(p->erate);
		}
		else{
			if (g==2) toadd = log(p->erate);
			else if (g==1) toadd = log(0.5);
			else if (g==0) toadd = 0.0;
		}
		if (i == 0) toreturn = toadd;
		else toreturn+=  toadd;
		//cout << snpi << " "<<g << " "<< toreturn << " "<< allele << " g\n";
	}

	return toreturn;
}

double Pileup::lemit_g(int which, int z1, int z2, int g){
	double toreturn;
	assert(g<3 && g>-1);
	double f1 = h->theta[which][z1];
	double f2 = h->theta[which][z2];
	if (g ==0) {
		toreturn = log(1-f1)+log(1-f2);
	}
	else if (g ==1){
		toreturn = log(f1*(1-f2)+f2*(1-f1));
	}
	else if (g==2){
		toreturn = log(f1)+log(f2);
	}
	return toreturn;
}
vector<double> Pileup::impute(int whichsnp, bool null){
	vector<double> toreturn(3, -1000000);
	if (whichsnp==0){
		int index =0;
		for (int i = 0; i < p->K; i++){
			for (int j = 0; j < p->K; j++){
				//cout << index << "\n";
				double lalpha = h->alpha[0][i]+ h->alpha[0][j];
				double lbwd = holdbwd[whichsnp][index];
				for (int k = 0; k <3; k++){
					double lgemit = lemit_g(whichsnp, i, j, k);
					double remit = readp_giveng(whichsnp, k);
					if (null) remit =0;
					double toadd = lgemit+remit+lalpha+lbwd -llk;
					toreturn[k] = h->sumlog(toreturn[k], toadd);
				}
				index++;
			}
		}
		return toreturn;
	}
	double rr = h->rrate[whichsnp];
	vector<double> fwdsums1(p->K, -1000000.0);
	vector<double> fwdsums2(p->K, -1000000.0);
	double allfwdsum = -1000000.0;
	int index = 0;
	for (int i = 0; i < p->K; i++){
		for (int j = 0; j < p->K; j++){
			double prevfwd = holdfwd[whichsnp-1][index];
			allfwdsum =  h->sumlog(allfwdsum, prevfwd);
			fwdsums1[i] = h->sumlog(fwdsums1[i], prevfwd);
			fwdsums2[j] = h->sumlog(fwdsums2[j], prevfwd);
			index++;
		}
	}
	index = 0;
	for (int j = 0; j < p->K; j++){
		for (int k = 0; k < p->K; k++){
			double term1= holdfwd[whichsnp-1][index] + 2*log(1-rr);
			double term2 = log(1-rr)+ log(rr)+ h->alpha[whichsnp][k] + fwdsums1[j];
			double term3 = log(1-rr)+ log(rr)+ h->alpha[whichsnp][j] + fwdsums2[k];
			double term4 = 2*log(rr)+ h->alpha[whichsnp][j] + h->alpha[whichsnp][k]+ allfwdsum;
			double allterms = h->sumlog(term1, term2);
			allterms = h->sumlog(allterms, term3);
			allterms = h->sumlog(allterms, term4);
			//cout << whichsnp << " "<< j <<  " "<< k << " "<< allterms << " "<< term1 << " "<< term2 << " "<< term3 << " "<< term4 << "\n";
			double lbwd = holdbwd[whichsnp][index];
			for (int l = 0; l <3; l++){
				double lgemit = lemit_g(whichsnp, j, k, l);

				double remit = readp_giveng(whichsnp, l);
				if (null) remit =0;
				//double remit = 0;
				double toadd = lgemit+remit+allterms+lbwd-llk;
				//if (whichsnp==133) cout << whichsnp << " "<< j << " "<< k << " "<< l << " "<< lgemit << " "<< remit <<  " "<< toadd << " "<< allterms << " "<< lbwd << " "<< llk <<" lg\n";
				toreturn[l] = h->sumlog(toreturn[l], toadd);
			}
			index++;
		}
	}

	return toreturn;
}

double Pileup::setbwd(bool null){
	// set forward variables
	double toreturn;
	assert(d_wmissing.size() == h->nsnp);
	//initialize
	int index =0;
	for (int i = 0; i < p->K; i++){
		for (int j = 0; j < p->K; j++){
			holdbwd[h->nsnp-1][index] = 0;
			index++;
		}
	}
	//recursion
	for (int i = h->nsnp-2; i >=0 ;i --){
		double rr = h->rrate[i+1];
		vector<double> bwdsums1(p->K, -1000000.0);
		vector<double> bwdsums2(p->K, -1000000.0);
		double allbwdsum = -1000000.0;
		index =0;
		for (int j = 0; j < p->K; j++){
			for (int k = 0; k < p->K; k++){
				double le = lemit(i+1, j, k);
				if (null) le =0;
				double prevbwd = holdbwd[i+1][index];
				bwdsums1[j]  = h->sumlog(bwdsums1[j], le+prevbwd+h->alpha[i+1][k]);
				bwdsums2[k]  = h->sumlog(bwdsums2[k], le+prevbwd+h->alpha[i+1][j]);
				allbwdsum = h->sumlog(allbwdsum, le+prevbwd+h->alpha[i+1][j]+h->alpha[i+1][k]);
				index++;
			}
		}
		index = 0;
		for (int j = 0; j < p->K; j++){
			for (int k = 0; k < p->K; k++){
				double le = lemit(i+1, j, k);
				if (null) le = 0;
				double prevbwd = holdbwd[i+1][index];
				double term1 = 2*log(1-rr) + le+prevbwd;
				double term2 = log(rr)+log(1-rr)+bwdsums1[j];
				double term3 = log(rr)+log(1-rr)+bwdsums2[k];
				double term4 = 2*log(rr)+ allbwdsum;
				double toadd = h->sumlog(term1, term2);
				toadd = h->sumlog(toadd, term3);
				toadd = h->sumlog(toadd, term4);
				holdbwd[i][index] = toadd;
				index++;
			}
		}
	}

	//termination
	index =0;
	for (int j = 0; j < p->K; j++){
		for (int k = 0; k < p->K; k++){
			double le = lemit(0, j, k);
			if (null) le = 0;
			double prevbwd = holdbwd[0][index];
			double aa = h->alpha[0][j]+h->alpha[0][k];
			if (index == 0) toreturn = le+prevbwd+aa;
			else toreturn = h->sumlog(toreturn, le+prevbwd+aa);
			index++;
		}
	}
	return toreturn;
}
void Pileup::read_infile(string infile){

	// read a pileup-style file
	// format is: [chromosome] [position] [ref] [count] [alleles]
	// not checking if genome build is the same as in the reference frequencies
	// checking to make sure the allele matches one of the reference alleles
	d.clear();
	info.clear();
	chr2pos2index.clear();
	string ext = infile.substr(infile.size()-3, 3);
	if (ext != ".gz"){
		std::cerr << infile << " is not gzipped (only .gz files accepted)\n";
		exit(1);
	}

	igzstream in(infile.c_str()); //only gzipped files
	vector<string> line;
	struct stat stFileInfo;
	int intStat;
	string st, buf;

	intStat = stat(infile.c_str(), &stFileInfo);
	if (intStat !=0){
		std::cerr<< "ERROR: cannot open file " << infile << "\n";
		exit(1);
	}

	int nwarning = 0;
	int index = 0;
	while(getline(in, st)){
		buf.clear();
		stringstream ss(st);
		line.clear();
		while (ss>> buf){
			line.push_back(buf);
		}



		if (line[0] == "#") continue;
		if (line.size() < 5) continue;


		//chr
		string chr = line[0];

		//pos
		int pos = atoi(line[1].c_str());


		if (h->chr2pos2indices.find(chr) != h->chr2pos2indices.end() and h->chr2pos2indices[chr].find(pos) != h->chr2pos2indices[chr].end()){

			vector<pair<string, string> > allals = h->get_als(make_pair(chr, pos));
			for (vector<pair<string, string> >::iterator it = allals.begin(); it != allals.end(); it++){
				pair<string, string> refals = *it;
				vector<bool> tmpals;
				// if we want to restrict to only snps
				bool snp = true;
				if (refals.first.size()> 1 or refals.second.size()>1) snp = false;
				// get the first allele
				string geno = line[4];
				for (int i = 0; i < geno.length(); i++){
					if (i >= p->maxreads) continue;
					string tmpgeno = geno.substr(i, 1);
					boost::to_upper(tmpgeno);

					if (tmpgeno == refals.first && snp) tmpals.push_back(false); //true = matches the alt allele
					else if (tmpgeno == refals.second && snp) tmpals.push_back(true);
					else {
						nwarning++;
					}
				}
				if(tmpals.size()> 0){
					als.push_back(refals);
					info.push_back(make_pair(chr, pos));
					d.push_back(tmpals);
					if(chr2pos2index.find(chr) == chr2pos2index.end()){
						map<int, int> tmp;
						chr2pos2index.insert(make_pair(chr, tmp));
					}
					chr2pos2index[chr].insert(make_pair(pos, index));
					index++;
				}
			}

		}
	}
	//cout << "WARNING: number of bad alleles "<< nwarning << " SNPs\n";
	nsnp = d.size();
}
