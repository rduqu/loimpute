# loimpute

This is software for genotype imputation from ultra-low-coverage sequencing data. It implements a version of [the fastPHASE model](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1424677/). 

The setup is that you have a large panel of phased whole genome sequences (e.g. from the 1000 Genomes Project) and a single sample with ultra-low-coverage sequencing (in our applications we usually have around 0.1-0.5x genome coverage) that you would like to impute. 

Our approach is in two stages--first, we learn the model parameters from the phased reference panel. Then, the model is applied to the single ultra-low-coverage sequencing sample for imputation. Importantly, the first step is the computationally-intensive step and needs to be done only once. Then then same model parameters can be applied to new samples as they arrive with minimal computation.

# Quick start

Note: requires the [GNU Scientific Library](https://www.gnu.org/software/gsl/), [Boost](http://www.boost.org/),  and [HTSLib](http://www.htslib.org/).  
```
#
# download and compile
#
git clone https://joepickrell@bitbucket.org/joepickrell/loimpute.git
cd loimpute/
./configure
make
#
# learn model parameters (or download them)
#
loimpute -h refpanel.vcf.gz  -o outparameters
#
# impute
#
loimpute -i inputpileup.mp.gz -p outparameters -o imputedvcf
```

# Input file format

The input file for imputation is gzipped output from [samtools](http://www.htslib.org/) mpileup command (run without a reference genome). This is just a list of chromosomes and positions along with the bases that cover the positions, like:

```
1	1330931	N	1	A	F
1	1596979	N	1	C	C
1	1779036	N	1	C	G
1	2338569	N	1	c	C
1	2339115	N	1	T	F
1	2792801	N	1	c	G
1	2896784	N	1	G	G
1	3044181	N	1	c	G
```

An example is available in test/NA11932.mp.gz and can be imputed with the command:

```
loimpute -i test/NA11932.mp.gz -p test/c20-eur-k10-it10
```
# Reference panel format

Phased [VCF](https://samtools.github.io/hts-specs/VCFv4.2.pdf). We have used the [phase 3 data](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/) from the 1000 Genomes Project.

# Output file format

Unphased, bgzipped [VCF](https://samtools.github.io/hts-specs/VCFv4.2.pdf). The GT field contains the best guess genotype, and the GL field contains genotype likelihoods (on a log10 scale, see VCF file specification).
